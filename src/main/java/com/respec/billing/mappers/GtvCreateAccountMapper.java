package com.respec.billing.mappers;

import com.respec.billing.accounts.models.GenericBillingProfile;
import com.respec.billing.accounts.models.GenericContact;
import com.respec.it.billing.gtv.client.model.*;
import org.mapstruct.*;
import com.respec.it.billing.gtv.client.model.BillType;
import org.mapstruct.factory.Mappers;

import com.respec.billing.accounts.models.GenericAccount;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvCreateAccountMapper {

    @Mapping(source = "category", target = "billingAccountCategory", qualifiedByName = { "billingAccountCategory" })
    @Mapping(source = "currency", target = "currencyCode", qualifiedByName = { "currencyCode" })
    @Mapping(source = "billingProfiles", target = "billCycle", qualifiedByName = { "billCycle" })
    @Mapping(source = "contacts", target = "contacts", qualifiedByName = { "contacts" })
    @Mapping(target = "billType", expression = "java(com.respec.it.billing.gtv.client.model.BillType.NONE)")
    @Mapping(target = "parentAccountRelation", source = "parentBillingAccount", qualifiedByName = { "parentBillingAccount" })
    @Mapping(source = "contacts", target = "responsibleParty", qualifiedByName = { "responsibleParty" })
    CreateCustomerBillingAccount convert(GenericAccount source);


    //@InheritInverseConfiguration
    //BillingAccount targetToSource(GenericAccount target);

    @Named("billingAccountCategory")
    default BillingAccountCategoryRef mapBillingAccountCategory(String id) {
        // DEFAULT NAME ON CREATE IS SET TO "Respec Billing Connector Default"
        BillingAccountCategoryRef billingAccountCategoryRef = new BillingAccountCategoryRef();
        billingAccountCategoryRef.setId(id);
        billingAccountCategoryRef.setName("Respec Billing Connector Default");
        return billingAccountCategoryRef;
    }

    @Named("currencyCode")
    default CurrencyCode mapCurrencyCode(String code) {
        // THIS IS CASE SENSITIVE AND MUST MATCH SO THAT
        // CurrencyCode.USD, CurrencyCode.AUD, CurrencyCode.GBP is translated!
        // e.g. "USD", "AUD", "GBP" etc.
        return CurrencyCode.valueOf(code);
    }

    @Named("billCycle")
    default MonthlyBillCycleRef mapBillCycle(List<GenericBillingProfile> profiles) {
        // DEFAULT IS ONE PROFILE / BILL CYCLE ON CREATE and it will be MONTHLY TYPE
        // that is until we know what 'Frequency' is defined to be in canonical model
        MonthlyBillCycleRef monthlyBillCycleRef = new MonthlyBillCycleRef();
        monthlyBillCycleRef.setId(profiles.get(0).getId());
        return monthlyBillCycleRef;
    }

    @Named("contacts")
    default List<CreateContact> mapContacts(List<GenericContact> genericContacts) {
        // DEFAULT CONTACT TYPE IS "BILLING CONTACT" SINCE WE HAVE NO
        // DEFINITIONS YET FOR contact types in canonical model
        List<CreateContact> contacts = new ArrayList<>();
        genericContacts.forEach((gc) -> {
            CreateContact c = new CreateContact();
            ContactCategoryRef contactCategoryRef = new ContactCategoryRef();
            //contactCategoryRef.setName("Billing Contact");
            // TYPES NEED TO MAP TO WHATEVER WE'VE CONFIGURED
            // IN GTV, IN THIS CASE:
            // 50 = Account Rep, 51 = Billing Contact and 52 = Sales Rep
            contactCategoryRef.setId(gc.getType());
            c.setContactCategory(contactCategoryRef);
            c.setFirstName(gc.getFirstName());
            c.setLastName(gc.getLastName());
            c.setCompany(gc.getCompany());
            List<CreateAddress> contactAddresses = new ArrayList<>();
            gc.getAddresses().forEach((ga) -> {
                CreatePostalAddress a = new CreatePostalAddress();
                a.setLine1(ga.getLine1());
                a.setCity(ga.getCity());
                a.setRegionOrState(ga.getState());
                a.setPostalCode(ga.getZip());
                // THIS SHOULD BE "POSTAL", "EMAIL" or "SERVICE"
                a.setAddressType(AddressType.valueOf(ga.getType()));
                // CANONICAL MODEL HAS NO CONCEPT OF 'PURPOSE'
                // VALID GTV VALUES ARE "BILLING", "SHIPPING", "SERVICE" and "PAYMENT"
                // (defaulting to  "BILLING")
                a.setPurpose(PostalAddressPurpose.BILLING);
                contactAddresses.add(a);
            });
            // THERE SHOULD BE 1 EMAIL ADDRESS ON
            // CREATE THAT DEFAULTS TO PRIMARY
            CreateEmailAddress e = new CreateEmailAddress();
            e.setAddressType(AddressType.EMAIL);
            e.setEmail(gc.getEmail());
            e.setPurpose(EmailAddressPurpose.PRIMARY);
            contactAddresses.add(e);
            c.setAddresses(contactAddresses);
            contacts.add(c);
        });
        return contacts;
    }

    @Named("billType")
    default BillType mapBillType() {
        // AT THIS TIME WE HAVE NO RULES / DEFINITIONS
        // TO HANDLE THIS GTV BILLING ACCOUNT CREATION
        //  REQUIREMENT. Valid values are:
        //  EMAIL, NONE, PAPER or PAPER_EMAIL
        return BillType.NONE;
    }

    @Named("responsibleParty")
    default CreateOrganization mapResponsibleParty(List<GenericContact> genericContacts) {
        // REUSING GENERIC CONTACT TO BUILD RESPONSIBLE PARTY
        // AS THERE IS NO CONCEPT OF RESPONSIBLE PARTY IN
        // CANONICAL MODEL
        CreateOrganization responsibleParty = new CreateOrganization();
        genericContacts.forEach((gc) -> {
            responsibleParty.setOrganizationName(gc.getCompany());
            List<CreateAddress> addresses = new ArrayList<>();
            gc.getAddresses().forEach((ga) -> {
                /* postal address */
                CreatePostalAddress postalAddress = new CreatePostalAddress();
                postalAddress.setLine1(ga.getLine1());
                postalAddress.setCity(ga.getCity());
                postalAddress.setRegionOrState(ga.getState());
                postalAddress.setPostalCode(ga.getZip());
                postalAddress.setAddressType(AddressType.POSTAL);
                postalAddress.setPurpose(PostalAddressPurpose.BILLING);
                addresses.add(postalAddress);
                /* email address */
                CreateEmailAddress emailAddress = new CreateEmailAddress();
                emailAddress.setEmail(gc.getEmail());
                emailAddress.setAddressType(AddressType.EMAIL);
                emailAddress.setPurpose(EmailAddressPurpose.PRIMARY);
                addresses.add(emailAddress);
            });
            responsibleParty.setAddresses(addresses);
        });
        return responsibleParty;
    }

    @Named("parentBillingAccount")
    default CreateParentAccountRelation mapParentBillingAccount(String parentBillingAccount) {
        CreateParentAccountRelation par = null;
        if (parentBillingAccount != null && !parentBillingAccount.isBlank()) {
            BillingAccountRef bar = new BillingAccountRef();
            bar.setAccountNum(parentBillingAccount);
            par = new CreateParentAccountRelation();
            par.setParentAccount(bar);
        }
        return par;
    }

}
