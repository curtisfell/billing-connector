package com.respec.billing.mappers;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

//import com.respec.billing.accounts.models.gtv.CpqDiscount;
import com.respec.it.billing.gtv.client.model.CreateCpqDiscount;
import com.respec.billing.accounts.models.GenericDiscount;
import org.springframework.core.convert.converter.Converter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { MapperUtility.class })
public interface GtvCreateCpqDiscountMapper {

    //@Mapping(source = "id", target = "id", qualifiedByName = { "id" })
    @Mapping(source = "startDate", target = "startDate", qualifiedByName = { "startDate" })
    //@Mapping(source = "deactivationDate", target = "deactivationDate", qualifiedByName = { "deactivationDate" })
    //@Mapping(source = "duration", target = "duration")
    //@Mapping(source = "recurrence", target = "recurrence")
    //@Mapping(source = "value", target = "value")
    //@Mapping(source = "applicationType", target = "applicationType")
    //@Mapping(source = "priceType", target = "priceType")
    //@Mapping(source = "discountKey", target = "discountKey")
    GenericDiscount convert(CreateCpqDiscount source);

    //@InheritInverseConfiguration
    //CpqDiscount targetToSource(GenericDiscount target);

    @Named("id")
    default String mapId(UUID id) {
        return id.toString();
    }

    @Named("startDate")
    default String mapStartDate(OffsetDateTime dt) {
        return dt.toString();
    }

    @Named("deactivationDate")
    default String mapDeactivationDate(OffsetDateTime dt) {
        return dt.toString();
    }

}
