package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

//import com.respec.billing.accounts.models.gtv.Address;
import com.respec.it.billing.gtv.client.model.CreatePostalAddress;
import com.respec.billing.accounts.models.GenericAddress;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = MapperUtility.class)
public interface GtvCreatePostalAddressMapper {

    //@Mapping(source = "addressType", target = "type")
    //@Mapping(source = "id", target = "id")
    //@Mapping(source = "country", target = "country")
    //@Mapping(source = "city", target = "city")
    //@Mapping(source = "line1", target = "line1")
    //@Mapping(source = "line2", target = "line2")
    //@Mapping(source = "regionOrState", target = "state")
    //@Mapping(source = "postalCode", target = "zip")
    GenericAddress convert(CreatePostalAddress source);

    //@InheritInverseConfiguration
    //PostalAddress targetToSource(GenericAddress target);

}
