package com.respec.billing.mappers;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.respec.it.billing.gtv.client.model.CreateBillCycle;
import com.respec.billing.accounts.models.GenericBillCycle;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GtvCreateBillCycleMapper {

    //@Mapping(source = "id", target = "id")
    //@Mapping(source = "billCycleType", target = "billCycleType")
    //@Mapping(source = "name", target = "name")
    GenericBillCycle convert(CreateBillCycle source);

    //@InheritInverseConfiguration
    //BillCycle targetToSource(GenericBillCycle target);

}
