package com.respec.billing.accounts.services;

public class AccountServiceException extends RuntimeException  {

    public AccountServiceException(String message) {
        super(message);
    }

    public AccountServiceException(String message, Throwable err) {
        super(message, err);
    }
}
