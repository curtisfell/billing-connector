package com.respec.billing.accounts.services;

import com.respec.it.billing.gtv.client.ApiException;

import java.util.List;

public interface AccountService<T> {

    void add(T o) throws ApiException;
    void update(T o) throws ApiException;
    void delete(T o) throws ApiException;
    T get(T o) throws ApiException;
    List<T> list(T o) throws ApiException;

}
