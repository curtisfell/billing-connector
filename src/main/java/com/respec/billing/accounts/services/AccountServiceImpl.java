package com.respec.billing.accounts.services;

import com.respec.billing.accounts.models.GenericAccount;
import com.respec.billing.common.GotransverseClient;
import com.respec.billing.mappers.GtvCreateAccountMapper;
import com.respec.it.billing.gtv.client.ApiException;
import com.respec.it.billing.gtv.client.model.BillingAccount;
import com.respec.it.billing.gtv.client.model.CreateCustomerBillingAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService<GenericAccount> {

    private final GtvCreateAccountMapper gtvCreateAccountMapper;
    private final GotransverseClient gotransverseClient;

    @Autowired
    public AccountServiceImpl(
            GtvCreateAccountMapper gtvCreateAccountMapper,
            GotransverseClient gotransverseClient
    ) {
        this.gtvCreateAccountMapper = gtvCreateAccountMapper;
        this.gotransverseClient = gotransverseClient;
    }

    @Override
    public void add(GenericAccount o) throws ApiException {

        String prefer = "FULL";
        List<String> expand = null;

        // Adding net-new GTV billing account requires using
        // CreateCustomerBillingAccount vs BillingAccount
        CreateCustomerBillingAccount cba = gtvCreateAccountMapper.convert(o);
        BillingAccount ba =
                gotransverseClient
                .getBillingAccountApi()
                .createBillingAccount(cba, prefer, expand);

        // TODO add logging

    }

    @Override
    public void update(GenericAccount o) throws ApiException {

    }

    @Override
    public void delete(GenericAccount o) throws ApiException {

    }

    @Override
    public GenericAccount get(GenericAccount o) throws ApiException {

        return null;
    }

    @Override
    public List<GenericAccount> list(GenericAccount o) throws ApiException {

        return null;
    }
}
