package com.respec.billing.accounts.exceptions;

public class BillingConnectorAccountException extends Exception {

  public BillingConnectorAccountException(String msg) {
    super(msg);
  }

  public BillingConnectorAccountException(String msg, Throwable cause) {
    super(msg, cause);
  }

} 
