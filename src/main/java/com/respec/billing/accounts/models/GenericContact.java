package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericContact implements Serializable {
    private String type;
    private String firstName;
    private String lastName;
    private String company;
    private List<GenericAddress> addresses = null;
    private List<GenericPhone> phones = null;
    private String email;
    private final static long serialVersionUID = -5328432171393434551L;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<GenericAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<GenericAddress> addresses) {
        this.addresses = addresses;
    }

    public List<GenericPhone> getPhones() {
        return phones;
    }

    public void setPhones(List<GenericPhone> phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
