package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.respec.billing.accounts.models.GenericDiscount;

public class GenericAccountRef implements Serializable
{
    private String accountNum;
    private String externalAccountNum;
    private List<GenericDiscount> genericDiscounts = null;
    private String id;
    private final static long serialVersionUID = 649055110452588068L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericAccountRef() {
    }

    /**
     *
     * @param accountNum
     * @param externalAccountNum
     * @param genericDiscounts
     * @param id
     */
    public GenericAccountRef(String accountNum, String externalAccountNum, List<GenericDiscount> genericDiscounts, String id) {
        super();
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.genericDiscounts = genericDiscounts;
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public List<GenericDiscount> getGenericDiscounts() {
        return genericDiscounts;
    }

    public void setGenericDiscounts(List<GenericDiscount> genericDiscounts) {
        this.genericDiscounts = genericDiscounts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
