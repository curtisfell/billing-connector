package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericPaymentMethod implements Serializable
{
    private String paymentMethodType;
    private String multiFactorAuth;
    private String id;

    public GenericPaymentMethod() {
    }

    public GenericPaymentMethod(String paymentMethodType, String multiFactorAuth, String id) {
        this.paymentMethodType = paymentMethodType;
        this.multiFactorAuth = multiFactorAuth;
        this.id = id;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getMultiFactorAuth() {
        return multiFactorAuth;
    }

    public void setMultiFactorAuth(String multiFactorAuth) {
        this.multiFactorAuth = multiFactorAuth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
