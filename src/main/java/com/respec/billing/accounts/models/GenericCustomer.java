package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import com.respec.billing.accounts.models.gtv.Address;
//import com.respec.billing.accounts.models.gtv.ContactCategory;
//import com.respec.billing.accounts.models.gtv.DefaultBillingAccount;

public class GenericCustomer implements Serializable
{

    @SerializedName("customer_num")
    @Expose
    private String customerNum;
    @SerializedName("external_customer_num")
    @Expose
    private String externalCustomerNum;
    @SerializedName("default_billing_account")
    @Expose
    private GenericAccountRef defaultBillingAccount;
    @SerializedName("tax_id_number")
    @Expose
    private String taxIdNumber;
    @SerializedName("addresses")
    @Expose
    private List<GenericAddress> addresses = null;
    @SerializedName("contact_category")
    @Expose
    private GenericContactCategory contactCategory;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 123481234478245559L;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericCustomer() {
    }

    /**
     *
     * @param contactCategory
     * @param addresses
     * @param defaultBillingAccount
     * @param taxIdNumber
     * @param customerNum
     * @param id
     * @param externalCustomerNum
     */
    public GenericCustomer(String customerNum, String externalCustomerNum, GenericAccountRef defaultBillingAccount, String taxIdNumber, List<GenericAddress> addresses, GenericContactCategory contactCategory, String id) {
        super();
        this.customerNum = customerNum;
        this.externalCustomerNum = externalCustomerNum;
        this.defaultBillingAccount = defaultBillingAccount;
        this.taxIdNumber = taxIdNumber;
        this.addresses = addresses;
        this.contactCategory = contactCategory;
        this.id = id;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(String customerNum) {
        this.customerNum = customerNum;
    }

    public String getExternalCustomerNum() {
        return externalCustomerNum;
    }

    public void setExternalCustomerNum(String externalCustomerNum) {
        this.externalCustomerNum = externalCustomerNum;
    }

    public GenericAccountRef getDefaultBillingAccount() {
        return defaultBillingAccount;
    }

    public void setDefaultBillingAccount(GenericAccountRef defaultBillingAccount) {
        this.defaultBillingAccount = defaultBillingAccount;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public List<GenericAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<GenericAddress> addresses) {
        this.addresses = addresses;
    }

    public GenericContactCategory getContactCategory() {
        return contactCategory;
    }

    public void setContactCategory(GenericContactCategory contactCategory) {
        this.contactCategory = contactCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
