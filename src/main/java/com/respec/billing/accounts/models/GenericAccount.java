package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class GenericAccount implements Serializable {

	private String accountNumber;
	private String category;
	private String currency;
	private String parentBillingAccount;
	private List<GenericContact> contacts;
	private List<GenericBillingProfile> billingProfiles;
	private List<GenericAdjustment> adjustments;
	private List<GenericFee> fees;
	private List<GenericService> services;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getParentBillingAccount() {
		return parentBillingAccount;
	}

	public void setParentBillingAccount(String parentBillingAccount) {
		this.parentBillingAccount = parentBillingAccount;
	}

	public List<GenericContact> getContacts() {
		return contacts;
	}

	public void setContacts(List<GenericContact> contacts) {
		this.contacts = contacts;
	}

	public List<GenericBillingProfile> getBillingProfiles() {
		return billingProfiles;
	}

	public void setBillingProfiles(List<GenericBillingProfile> billingProfiles) {
		this.billingProfiles = billingProfiles;
	}

	public List<GenericAdjustment> getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(List<GenericAdjustment> adjustments) {
		this.adjustments = adjustments;
	}

	public List<GenericFee> getFees() {
		return fees;
	}

	public void setFees(List<GenericFee> fees) {
		this.fees = fees;
	}

	public List<GenericService> getServices() {
		return services;
	}

	public void setServices(List<GenericService> services) {
		this.services = services;
	}
}
