package com.respec.billing.accounts.models;

import java.io.Serializable;
import java.util.List;

//TODO this is broken
//public abstract class BaseAccount <T, K extends BaseAccount <T, K>> implements Serializable {
//public abstract class BaseAccount <T extends BaseAccount <T>> implements Serializable {
public abstract class BaseAccount implements Serializable {

	private String accountId;
	private String parentAccountId;
	private String type;
	private int currencyCode;

	public BaseAccount(){}

	public BaseAccount(String accountId, String parentAccountId, String type, int currencyCode) {
		this.accountId = accountId;
		this.parentAccountId = parentAccountId;
		this.type = type;
		this.currencyCode = currencyCode;
	}

/*	this stuff is broken...

	public T map(K k) {
		//TODO - mapping so we don't implement in each subclass
		return (T) this;
	}

	public List<T> map(List<K> k) {
		//TODO - mapping so we don't implement in each subclass
		return (List<T>) this;
	}

*/

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getParentAccountId() {
		return parentAccountId;
	}

	public void setParentAccountId(String parentAccountId) {
		this.parentAccountId = parentAccountId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(int currencyCode) {
		this.currencyCode = currencyCode;
	}

}
