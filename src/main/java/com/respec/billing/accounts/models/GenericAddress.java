package com.respec.billing.accounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericAddress implements Serializable
{
    private String type;
    private String id;
    private String country;
    private String city;
    private String line1;
    private String line2;
    private String state;
    private String zip;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericAddress() {
    }

    /**
     *
     * @param country
     * @param purpose
     * @param city
     * @param addressType
     * @param regionOrState
     * @param postalCode
     * @param id
     * @param line2
     * @param line1
     * @param email
     */
    public GenericAddress(String addressType, String id, String purpose, String country, String city, String line1, String line2, String regionOrState, String postalCode, String email) {
        super();
        this.type = type;
        this.id = id;
        this.country = country;
        this.city = city;
        this.line1 = line1;
        this.line2 = line2;
        this.state = state;
        this.zip = zip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
