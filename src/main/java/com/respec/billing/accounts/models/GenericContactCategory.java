package com.respec.billing.accounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenericContactCategory implements Serializable
{
    private String name;
    private String description;
    private String status;
    private String id;

    /**
     * No args constructor for use in serialization
     *
     */
    public GenericContactCategory() {
    }

    /**
     *
     * @param name
     * @param description
     * @param id
     * @param status
     */
    public GenericContactCategory(String name, String description, String status, String id) {
        super();
        this.name = name;
        this.description = description;
        this.status = status;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
