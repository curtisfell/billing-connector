package com.respec.billing.accounts.models;

import java.io.Serializable;

public class GenericPhone implements Serializable {

    private String type;
    private String number;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
