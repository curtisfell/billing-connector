package com.respec.billing.accounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationContextInitializedEvent;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.EventListener;

@SpringBootApplication(scanBasePackages = {
        "com.respec.billing.accounts.contracts",
        "com.respec.billing.accounts.models",
        "com.respec.billing.accounts.repositories",
        "com.respec.billing.accounts.services",
        "com.respec.billing.common",
        "com.respec.billing.mappers",
        "com.respec.billing"
})
//@Import(SpringDataRestConfiguration.class)
public class AccountsApplication
{
    public static void main(String[] args)
    {
        //LoggerFactory.getApacheLogger().setLevel(java.util.logging.Level.WARNING);

        SpringApplication.run(AccountsApplication.class, args);
    }

    @EventListener(ApplicationStartingEvent.class)
    public void onApplicationStartingEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION STARTING: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationEnvironmentPreparedEvent.class)
    public void onApplicationEnvironmentPreparedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION ENVIRONMENT PREPARED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationContextInitializedEvent.class)
    public void onApplicationContextInitializedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION CONTEXT INITIALIZED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationPreparedEvent.class)
    public void onApplicationPreparedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION PREPARED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationStartedEvent.class)
    public void onApplicationStartedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION STARTED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationReadyEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION READY: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationFailedEvent.class)
    public void onApplicationFailedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).error().message(String.format("[EVENT] APPLICATION FAILED TO START: %s", this.getClass().getSimpleName())).log();
    }
}
