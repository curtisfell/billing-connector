package com.respec.billing.accounts.contracts;

import com.respec.billing.accounts.models.GenericAccount;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@SuppressWarnings({"PMD.UnnecessaryAnnotationValueElement", "PMD.UseObjectForClearerAPI"})
public interface AccountsProxy
{
    @RequestMapping(
            value = "/api/billing/account-services/{tenant}/list-accounts",
            method = RequestMethod.GET)
    ResponseEntity<List<GenericAccount>> listAccountsByTenantId(
            @PathVariable(value = "tenant") final String tenantId,
            @RequestHeader HttpHeaders requestHeaders);

    @RequestMapping(
            value = "/api/billing/account-services/{tenant}/get-account/{accountid}",
            method = RequestMethod.GET)
    ResponseEntity<GenericAccount> getAccountById(
            @PathVariable(value = "tenant") final String tenantId,
            @PathVariable(value = "accountid") final int accountId,
            @RequestHeader HttpHeaders requestHeaders);

}
