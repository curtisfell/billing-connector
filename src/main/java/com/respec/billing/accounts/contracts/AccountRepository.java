package com.respec.billing.accounts.contracts;

import java.util.List;

public interface AccountRepository<T>
{
   List<T> listAccountsByTenantId(String tenantId);
   T getAccountById(String tenantId, int accountId);
}
