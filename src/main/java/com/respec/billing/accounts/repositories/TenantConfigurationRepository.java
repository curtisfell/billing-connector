package com.respec.billing.accounts.repositories;

import com.respec.billing.accounts.contracts.ConfigurationRepository;
import com.respec.billing.common.AccountConfiguration;
import org.springframework.stereotype.Repository;

@Repository
public class TenantConfigurationRepository implements ConfigurationRepository<AccountConfiguration> {

    @Override
    public AccountConfiguration getConfigurationByTenantId(String tenantId) {

        AccountConfiguration conf = new AccountConfiguration(tenantId);
        if (tenantId.equalsIgnoreCase("1"))
            conf.setProvider(AccountConfiguration.BillingProvider.GTV);
        conf.setProvider(AccountConfiguration.BillingProvider.BRM);

        //TODO what do we need here?

        return conf;
    }
}
