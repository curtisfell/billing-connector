package com.respec.billing.common;

public class HttpResponseObject
{
    private String responseString;
    private boolean isSuccessful;

    public HttpResponseObject(){};

    public String getResponseString()
    {
        return this.responseString;
    }

    public void setResponseString(final String responseString)
    {
        this.responseString = responseString;
    }

    public boolean isSuccessful()
    {
        return this.isSuccessful;
    }

    public void setSuccessful(final boolean successful)
    {
        this.isSuccessful = successful;
    }
}
