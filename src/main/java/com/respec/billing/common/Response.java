package com.respec.billing.common;

import com.fasterxml.jackson.annotation.JsonProperty;
//import com.respec.billing.accounts.contracts.IAccount;
import com.respec.billing.accounts.models.BaseAccount;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Response<T> //extends BaseMessage
{
    private boolean isSuccessful;
    private List<T> accounts;
    private String rawData;
    private String tenantId;
    private String timestamp;
    private HashMap<String,String> errors;
    private HashMap<String, String> additionalProperties;

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public List<T> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<T> accounts) {
        this.accounts = accounts;
    }

    public String getRawData() {
        return rawData;
    }

    public void setRawData(String rawData) {
        this.rawData = rawData;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public HashMap<String, String> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, String> errors) {
        this.errors = errors;
    }

    public HashMap<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Response()
    {
        this.errors = new HashMap<>();
        this.additionalProperties = new HashMap<>();
    }

    public Response(boolean isSuccessful, String rawData)
    {
        this.errors = new HashMap<>();
        this.additionalProperties = new HashMap<>();
        this.isSuccessful = isSuccessful;
        this.rawData = rawData;
    }

    public Response(boolean isSuccessful, List<T> accounts)
    {
        this.errors = new HashMap<>();
        this.additionalProperties = new HashMap<>();
        this.isSuccessful = isSuccessful;
        this.accounts = accounts;
    }

}
