package com.respec.billing.common;

import com.respec.it.billing.gtv.client.ApiClient;
import com.respec.it.billing.gtv.client.ApiException;
import com.respec.it.billing.gtv.client.Configuration;
import com.respec.it.billing.gtv.client.api.ContactApi;
import com.respec.it.billing.gtv.client.api.RecurringPaymentApi;
import com.respec.it.billing.gtv.client.auth.*;
import com.respec.it.billing.gtv.client.api.BillingAccountApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GotransverseClient {

    @Value("${gtv-config.port}")
    private String port;
    @Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    /* this is doo-doo, need to figure out what was broke to prevent initializing... */
    @Value("${gtv-config.api-key}")
    private String gtvApiKey = "e599ab3130ee4b2b9f12436400a57d13";
    //@Value("${gtv-config.api.base.url}")  /* Direct to GTV - no tunnel */
    @Value("${gtv-config.api.base.url.test}")  /* tunnel to GTV for testing */
    private String billingAccountsUrl="http://localhost:4444/billing/2";

    private ApiClient defaultClient;

    public GotransverseClient() {

        defaultClient = Configuration.getDefaultApiClient();
        ApiKeyAuth api_key_header = (ApiKeyAuth) defaultClient.getAuthentication("api_key_header");
        api_key_header.setApiKey(gtvApiKey);
        ApiKeyAuth defined_bearer_token = (ApiKeyAuth) defaultClient.getAuthentication("defined_bearer_token");
        defined_bearer_token.setApiKey(gtvApiKey);
        defaultClient.setApiKey(gtvApiKey);

    }

    public ApiClient getDefaultClient() {

        return defaultClient;

    }

    public BillingAccountApi getBillingAccountApi() {

        ApiClient apc = getDefaultClient();
        apc.setApiKey(gtvApiKey);
        apc.setBasePath(billingAccountsUrl);
        return new BillingAccountApi(apc);

    }

    public ContactApi getContactApi() {

        ApiClient apc = getDefaultClient();
        apc.setApiKey(gtvApiKey);
        apc.setBasePath(billingAccountsUrl);
        return new ContactApi(apc);

    }


    public RecurringPaymentApi getRecurringPaymentApi() {

        ApiClient apc = getDefaultClient();
        apc.setApiKey(gtvApiKey);
        apc.setBasePath(billingAccountsUrl);
        return new RecurringPaymentApi(apc);

    }

}
