package com.respec.billing.accounts.services;

import com.respec.billing.accounts.models.GenericAccount;
import com.respec.billing.accounts.models.GenericAddress;
import com.respec.billing.accounts.models.GenericBillingProfile;
import com.respec.billing.accounts.models.GenericContact;
import com.respec.billing.common.GotransverseClient;
import com.respec.billing.mappers.GtvCreateAccountMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AccountServiceImplTest {

    //@Value("${gtv-config.port}")
    private String port = "4444";
    //@Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;
    //@Value("${gtv-config.api.path.billing-accounts}")  // billing-accounts path
    private String billingAccountsPath;
    //@Value("${gtv-config.api.path.billing-account-categories}")  // billing-accounts-categories
    private String billingAccountCategories;

    GenericAccount genericAccount;

    @BeforeEach
    void setUp() {

        String DEFAULT_ACCOUNT_CATEGORY = "315";
        String DEFAULT_ACCOUNT_CURRENCY = "USD";
        String DEFAULT_CONTACT_TYPE = "51"; /* Billing Contact */
        String CONTACT_ADDRESS1_TYPE = "POSTAL"; /* POSTAL, EMAIL, TELECOM */
        String DEFAULT_BILLING_PROFILE = "4004"; /* translates to a GTV monthly bill cycle */

        genericAccount = new GenericAccount();

        genericAccount.setCategory(DEFAULT_ACCOUNT_CATEGORY);
        genericAccount.setCurrency(DEFAULT_ACCOUNT_CURRENCY);
        /* contact */
        List<GenericContact> contacts = new ArrayList<>();
        GenericContact gc = new GenericContact();
        gc.setType(DEFAULT_CONTACT_TYPE);
        gc.setFirstName("Donna");
        gc.setLastName("Summer");
        gc.setCompany("Last Dance Studios I have no Parent");
        gc.setEmail("donna.summer@lds.com");
        /* contact addresses */
        List<GenericAddress> addresses = new ArrayList<>();
        GenericAddress address1 = new GenericAddress();
        address1.setLine1("2600 Tiburon Dr");
        address1.setCity("Naples");
        address1.setState("FL");
        address1.setCountry("USA");
        address1.setZip("34109");
        address1.setType(CONTACT_ADDRESS1_TYPE);
        addresses.add(address1);
        gc.setAddresses(addresses);
        contacts.add(gc);
        genericAccount.setContacts(contacts);
        /* billing profiles */
        List<GenericBillingProfile> profiles = new ArrayList<>();
        GenericBillingProfile gbp = new GenericBillingProfile();
        gbp.setId(DEFAULT_BILLING_PROFILE);
        profiles.add(gbp);
        genericAccount.setBillingProfiles(profiles);

    }

    @Test
    void map_generic_account_to_billing_account_no_parent_test() {

        GtvCreateAccountMapperImpl mapper = new GtvCreateAccountMapperImpl();
        GotransverseClient client = new GotransverseClient();
        AccountServiceImpl service = new AccountServiceImpl(mapper, client);
        try {
            service.add(genericAccount);
        }
        catch (Exception e) {
            int catch_it = 1;
        }

        int stop_it = 1;

    }

    @Test
    void map_generic_account_to_billing_account_with_parent_test() {

        GtvCreateAccountMapperImpl mapper = new GtvCreateAccountMapperImpl();
        GotransverseClient client = new GotransverseClient();
        AccountServiceImpl service = new AccountServiceImpl(mapper, client);
        try {
            genericAccount.setParentBillingAccount("122");
            service.add(genericAccount);
        }
        catch (Exception e) {
            int catch_it = 1;
        }

        int stop_it = 1;

    }
}