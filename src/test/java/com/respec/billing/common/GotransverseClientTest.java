package com.respec.billing.common;

import com.respec.billing.accounts.AccountsApplication;
import com.respec.it.billing.gtv.client.ApiClient;
import com.respec.it.billing.gtv.client.ApiException;
import com.respec.it.billing.gtv.client.api.BillingAccountApi;
import com.respec.it.billing.gtv.client.auth.ApiKeyAuth;
import com.respec.it.billing.gtv.client.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = AccountsApplication.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@EnableAutoConfiguration
class GotransverseClientTest {

    @Autowired
    private MockMvc mockMvc;

    //@Value("${gtv-config.port}")
    private String port;
    //@Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    //@Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;

    @BeforeEach
    void setUp() { }

    @Test
    public void create_billing_account_api_client_test() {

        String prefer = "FULL";
        //Valid expand fields: tax_exemptions, cpq_discounts, responsible_party, effective_payment_term
        //List<String> expand = Arrays.asList("tax_exemptions", "cpq_discounts", "responsible_party", "effective_payment_term");
        //List<String> expand = Arrays.asList("responsible_party");
        List<String> expand = null;

        // Initialize client
        GotransverseClient gtvc = new GotransverseClient();
        ApiClient apc = gtvc.getDefaultClient();
        apc.setApiKey(gtvApiKey);
        apc.setBasePath(billingAccountsUrl);
        ApiKeyAuth api_key_header = (ApiKeyAuth) apc.getAuthentication("api_key_header");
        api_key_header.setApiKey(gtvApiKey);
        ApiKeyAuth defined_bearer_token = (ApiKeyAuth) apc.getAuthentication("defined_bearer_token");
        defined_bearer_token.setApiKey(gtvApiKey);
        BillingAccountApi apiInstance = new BillingAccountApi(apc);

        // Billing Account
        CreateCustomerBillingAccount createCustomerBillingAccount = new CreateCustomerBillingAccount();

        // Bill Cycle
        MonthlyBillCycleRef monthlyBillCycleRef = new MonthlyBillCycleRef();
        monthlyBillCycleRef.setId("4004");
        createCustomerBillingAccount.setBillCycle(monthlyBillCycleRef);

        // Account Category
        BillingAccountCategoryRef billingAccountCategoryRef = new BillingAccountCategoryRef();
        billingAccountCategoryRef.setId("315");
        billingAccountCategoryRef.setName("Respec Billing Connector");
        createCustomerBillingAccount.setBillingAccountCategory(billingAccountCategoryRef);

        // Responsible Party
        CreateOrganization responsibleParty = new CreateOrganization();
        responsibleParty.setOrganizationName("Respec Billing Connector Testing LLC");
        List<CreateAddress> addresses = new ArrayList<>();
        /* postal address */
        CreatePostalAddress postalAddress = new CreatePostalAddress();
        postalAddress.setLine1("700 International Pkwy");
        postalAddress.setCity("Richardson");
        postalAddress.setRegionOrState("TX");
        postalAddress.setPostalCode("75201");
        postalAddress.setAddressType(AddressType.POSTAL);
        postalAddress.setPurpose(PostalAddressPurpose.BILLING);
        addresses.add(postalAddress);
        /* email address */
        CreateEmailAddress emailAddress = new CreateEmailAddress();
        emailAddress.setEmail("billing-connector@respec.com");
        emailAddress.setAddressType(AddressType.EMAIL);
        emailAddress.setPurpose(EmailAddressPurpose.PRIMARY);
        addresses.add(emailAddress);
        responsibleParty.setAddresses(addresses);
        createCustomerBillingAccount.setResponsibleParty(responsibleParty);

        // Contacts
        List<CreateContact> contacts = new ArrayList<>();
        CreateContact contact = new CreateContact();
        contact.setCompany("Respec Billing Connector Testing LLC");
        contact.firstName("Karen");
        contact.setLastName("Karensinson");
        List<CreateAddress> contactAddresses = new ArrayList<>();
        /* contact email address */
        CreateEmailAddress contactEmailAddress = new CreateEmailAddress();
        contactEmailAddress.setAddressType(AddressType.EMAIL);
        contactEmailAddress.setEmail("karen.karensinson@respec.com");
        contactEmailAddress.setPurpose(EmailAddressPurpose.PRIMARY);
        contactAddresses.add(contactEmailAddress);
        contact.setAddresses(contactAddresses);
        /* contact category */
        ContactCategoryRef contactCategoryRef = new ContactCategoryRef();
        contactCategoryRef.setName("Billing Contact");
        contactCategoryRef.setId("51");
        contact.setContactCategory(contactCategoryRef);
        contacts.add(contact);
        createCustomerBillingAccount.setContacts(contacts);

        // Default Currency Code
        createCustomerBillingAccount.setCurrencyCode(CurrencyCode.USD);

        // Bill Type
        createCustomerBillingAccount.setBillType(BillType.EMAIL);

        // Payment
        /* recurring payment */
        CreateRecurringPayment recurringPayment = new CreateRecurringPayment();
        /* payment method */
        CreateTokenizedCreditCardPaymentMethod paymentMethod = new CreateTokenizedCreditCardPaymentMethod();
        paymentMethod.setNickname("Credit Card Ending 1111");
        paymentMethod.setToken("175760009");
        paymentMethod.setTokenTwo("8gg27pb");
        paymentMethod.setMultiFactorAuth(false);
        paymentMethod.setCardType(CardType.VISA);
        paymentMethod.setLastFour("1111");
        paymentMethod.setFirstName("Jack");
        paymentMethod.setLastName("White");
        paymentMethod.setIdentifierNumber("Last 4: 1111");
        paymentMethod.setExpirationDate("11/2004");
        /* do we need postal or email address? */
        /* apparently not so what are biz reqs? */
        recurringPayment.setPaymentMethod(paymentMethod);
        recurringPayment.setAutoPayment(false);
        createCustomerBillingAccount.setRecurringPayment(recurringPayment);

/*
        BillingAccount result;
        try {
            result = apiInstance.createBillingAccount(createCustomerBillingAccount, prefer, expand);
            int x = 1;
        }
        catch (ApiException e1) {
            int y = 0;
        }
        catch (Exception e2) {
            int y = 0;
        }
*/
        int stop = 1;

    }
}
